package com.example.userapi.application.service;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.application.port.in.UserUseCase;
import com.example.userapi.application.port.out.UserPersistencePort;
import com.example.userapi.domain.User;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService implements UserUseCase {

    private final UserPersistencePort userPersistencePort;

    public UserService(UserPersistencePort userPersistencePort) {
        this.userPersistencePort = userPersistencePort;
    }


    @Override
    public String saveUser(UserEntity user) {
        return userPersistencePort.save(user);
    }

    @Override
    public String updateUser(UserEntity userEntity) {
        return userPersistencePort.update(userEntity);
    }

    @Override
    public String getUser(String userId) {
        return userPersistencePort.findByUserId(userId).toString();
    }

    @Override
    public String checkExistingUserId(String userId) {
        if(userPersistencePort.findByUserId(userId) != null) {
            return userPersistencePort.findByUserId(userId).toString();
        }
        else {
            return null;
        }

    }

    @Override
    public String checkIfPasswordMatches(String userId, String password) {
        if (userPersistencePort.findByUserIdEntity(userId).getPassword().equals(password)) {
            return "Password matches, Login Successful";
        } else {
            return "Password does not match";
        }
    }

    @Override
    public Boolean checkIfPasswordMatchesForgotPassword(String userId, String password) {
        return userPersistencePort.findByUserIdEntity(userId).getPassword().equals(password);
    }


}
