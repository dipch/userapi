package com.example.userapi.application.port.out;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.domain.User;

public interface UserPersistencePort {
    String save(UserEntity user);

    String update(UserEntity userEntity);

    String findByUserId(String userId);

    UserEntity findByUserIdEntity(String userId);

    String getPassword(String userId);

}
