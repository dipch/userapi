package com.example.userapi.application.port.in;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.domain.User;

public interface UserUseCase {
    String saveUser(UserEntity user);

    String updateUser(UserEntity userEntity);

    String getUser(String userId);

    String checkExistingUserId(String userId);

    String checkIfPasswordMatches(String userId, String password);

    Boolean checkIfPasswordMatchesForgotPassword(String userId, String password);




}
