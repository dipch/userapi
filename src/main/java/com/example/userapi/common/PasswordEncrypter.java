package com.example.userapi.common;

import org.jasypt.encryption.pbe.StandardPBEStringEncryptor;

public class PasswordEncrypter {

    String seed = "dip";

    public String encrypt(String password) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(seed);
        return encryptor.encrypt(password);
    }
    public String decrypt(String password) {
        StandardPBEStringEncryptor encryptor = new StandardPBEStringEncryptor();
        encryptor.setPassword(seed);
        return encryptor.decrypt(password);
    }
}
