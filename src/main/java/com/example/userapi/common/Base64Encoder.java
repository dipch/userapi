package com.example.userapi.common;

import java.util.Base64;

public class Base64Encoder {

    public String encrypt(String password){
        Base64.Encoder enc = Base64.getEncoder();
        return enc.encodeToString(password.getBytes());
    }
    public String decrypt(String password){
        Base64.Decoder dec = Base64.getDecoder();
        return new String(dec.decode(password));
    }

}
