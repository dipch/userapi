package com.example.userapi.adapter.in.web.controllers;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.application.port.in.UserUseCase;
import com.example.userapi.common.Base64Encoder;
import com.example.userapi.common.PasswordEncrypter;
import com.example.userapi.domain.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;


import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Validated
@RestController
@Slf4j
public class UserController {
    private final UserUseCase userUseCase;

    Base64Encoder passwordEncrypter = new Base64Encoder();






    public UserController(UserUseCase userUseCase) {
        this.userUseCase = userUseCase;
    }

    @GetMapping("/v1/user/view-info")
    public ResponseEntity<String> viewUserInfo(@RequestParam(required = true) String userId) {
        String userMessage = userUseCase.getUser(userId);
        return ResponseEntity.ok(userMessage);
    }

    @PostMapping("/v1/user/register")
    public ResponseEntity<String> registerUser(@RequestParam(required = true)  String userId,
                                             @RequestParam(required = true) String password,
                                             @RequestParam(required = true) String mobileNo)  {
        log.info("==========================================================================");
        log.info("New User Registration Request Received for UserId: {}, Password: {}, MobileNo: {}", userId, password, mobileNo);
        if((userUseCase.checkExistingUserId(userId)) == null) {



            String encryptedPassword = passwordEncrypter.encrypt(password);
            log.info("Encrypted Password: {}", encryptedPassword);

            String userMessage = userUseCase.saveUser(new UserEntity(userId, encryptedPassword, mobileNo));
            log.info("New User Registration Request Completed" );
            log.info("==========================================================================");
            return ResponseEntity.ok(userMessage);
        }
        else {
            log.info("UserId already exists");
            log.info("==========================================================================");
            return ResponseEntity.badRequest().body("UserId already exists");
        }


    }


    @PostMapping("/v1/user/forgot-password")
    public ResponseEntity<String> updateUser(@RequestParam(required = true)  String userId,
                                               @RequestParam(required = true) String password,
                                               @RequestParam(required = true) String newPassword,
                                               @RequestParam(required = true) String mobileNo)  {
        log.info("==========================================================================");
        log.info("Forgot Password Request Received for UserId: {}, Old Password: {}, New Password: {}, MobileNo: {}", userId, password, newPassword,mobileNo);
        String encryptedPassword = passwordEncrypter.encrypt(password);
        String encryptedPassword2 = passwordEncrypter.encrypt(newPassword);
        if((userUseCase.checkExistingUserId(userId)) == null) {
            log.info("UserId does not exist");
            log.info("==========================================================================");
            return ResponseEntity.badRequest().body("UserId does not exist");

        } else {
            if ( (userUseCase.checkIfPasswordMatchesForgotPassword(userId, encryptedPassword))) {
                String userMessage = userUseCase.updateUser(new UserEntity(userId, encryptedPassword2, mobileNo));
                log.info("Forgot Password Request Completed");
                log.info("==========================================================================");
                return ResponseEntity.ok(userMessage);
            } else {
                log.info("Old Password does not match");
                log.info("==========================================================================");
                return ResponseEntity.badRequest().body("Old Password does not match");
            }
        }

//        } else{
//            String userMessage = userUseCase.updateUser(new UserEntity(userId, password, mobileNo));
//            log.info("Forgot Password Request Completed" );
//            log.info("==========================================================================");
//            return ResponseEntity.ok(userMessage);
//        }
    }

    @PostMapping("/v1/user/login")
    public ResponseEntity<String> loginUser(@RequestParam(required = true)  String userId,
                                            @RequestParam(required = true) String password)  {
        log.info("==========================================================================");
        log.info("Login Request Received for UserId: {}, Password: {}", userId, password);
        if((userUseCase.checkExistingUserId(userId)) == null) {
            log.info("UserId does not exist");
            log.info("==========================================================================");
            return ResponseEntity.badRequest().body("UserId does not exist");

        }
        else{
            String encryptedPassword = passwordEncrypter.encrypt(password);
            log.info("Encrypted Input Password: {}", encryptedPassword);
            log.info(userUseCase.checkIfPasswordMatches(userId, encryptedPassword));
            log.info("==========================================================================");
            return ResponseEntity.ok(userUseCase.checkIfPasswordMatches(userId, encryptedPassword));
        }

    }




}
