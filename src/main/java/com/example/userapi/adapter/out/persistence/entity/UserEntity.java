package com.example.userapi.adapter.out.persistence.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Builder
@Entity
@Table(name = "users")
public class UserEntity {
    @Id
    @Column(name = "userid")
    @NotBlank(message = "UserId is mandatory")
    @Size(min = 6, message = "UserId should be should be at least 6 characters")
    @Size(max = 6, message = "UserId should be should be at most 6 characters")
    private String userId;

    @Column(name = "password")
    @NotBlank(message = "Password is mandatory")
    //@Min(value = 6, message = "Password should be at least 6 characters")
    @Size(min = 6, message = "Password should be at least 6 characters")
    private String password;

    @Column(name = "mobileNo")
    @NotBlank(message = "MobileNo is mandatory")
    @Size(min = 11, message = "Mobile No should be at least 11 digits")
    @Size(max = 11, message = "Mobile No should be at most 11 digits")
    private String mobileNo;

    public UserEntity(String userId, String password, String mobileNo) {
        this.userId = userId;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    public UserEntity() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UserId: ").append(userId);
        sb.append(" Password: ").append(password);
        sb.append(" MobileNo: ").append(mobileNo);
        return sb.toString();
    }

}
