package com.example.userapi.adapter.out.persistence.repository;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {
    UserEntity findByUserId(String userId);
}

