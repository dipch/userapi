package com.example.userapi.adapter.out.persistence;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.adapter.out.persistence.repository.UserRepository;
import com.example.userapi.application.port.out.UserPersistencePort;
import com.example.userapi.domain.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class UserPersistenceAdapter implements UserPersistencePort {

    @Autowired
    private UserRepository userRepository;
//
//    public UserPersistenceAdapter(UserRepository userRepository) {
//        this.userRepository = userRepository;
//    }

    @Override
    public String save(UserEntity user) {
        userRepository.save(user);
        return user.toString();
    }

    @Override
    public String update(UserEntity userEntity) {
        userRepository.save(userEntity);
        return userEntity.toString();
    }

    @Override
    public String findByUserId(String userId) {
        if(userRepository.existsById(userId)) {
            return userRepository.findById(userId).toString();
        }
        else {
            return  null;
        }
    }

    @Override
    public UserEntity findByUserIdEntity(String userId) {
        if(userRepository.existsById(userId)) {
            return userRepository.findByUserId(userId);
        }
        else {
            return null;
        }

    }

    @Override
    public String getPassword(String userId) {
        return userRepository.findByUserId(userId).getPassword();
    }



}


