package com.example.userapi.domain;

import lombok.*;

@Data
@Builder
public class User {
    private String userId;
    private String password;
    private String mobileNo;

    public User(String userId, String password, String mobileNo) {
        this.userId = userId;
        this.password = password;
        this.mobileNo = mobileNo;
    }

    public User() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("UserId: ").append(userId).append("\n");
        sb.append("Password: ").append(password).append("\n");
        sb.append("MobileNo: ").append(mobileNo).append("\n");
        return sb.toString();
    }
}
