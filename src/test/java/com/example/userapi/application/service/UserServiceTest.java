package com.example.userapi.application.service;

import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.application.port.out.UserPersistencePort;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Mock
    UserPersistencePort userPersistencePort;

    @InjectMocks
    UserService userService;

//    @Test
//    void GivenUserEntityWhenSaveUserIsCalledShouldReturnString() {
//        //given
//
//
//        UserEntity userEntity = new UserEntity("111111", "222222", "01521308952");
//
//        //when
//        Mockito.when(userPersistencePort.save(new UserEntity("111111", "222222", "01521308952"))).thenReturn("expectedUserId");
//
//        //then
//        assertThat(userService.saveUser(userEntity)).isEqualTo("expectedUserId");
//    }

//    @Test
//    void updateUser() {
//    }
//
//    @Test
//    void getUser() {
//    }
//
//    @Test
//    void checkExistingUserId() {
//    }
//
//    @Test
//    void checkIfPasswordMatches() {
//    }
//
//    @Test
//    void checkIfPasswordMatchesForgotPassword() {
//    }
}