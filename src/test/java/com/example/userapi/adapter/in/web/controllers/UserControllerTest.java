package com.example.userapi.adapter.in.web.controllers;

import com.example.userapi.adapter.out.persistence.UserPersistenceAdapter;
import com.example.userapi.adapter.out.persistence.entity.UserEntity;
import com.example.userapi.application.port.in.UserUseCase;
import com.example.userapi.common.Base64Encoder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    UserPersistenceAdapter userPersistenceAdapter;

    private UserUseCase userUseCase;

    private Base64Encoder base64Encoder;

    private static final String userId = "111111";
    private static final String password = "222222";
    private static final String mobileNo = "01521308952";

    private static final String CONTENT_TYPE = "application/json";


    @Test
    void GivenUserCredentianls_WhenRegisterUserIsCalled_ShouldReturnResponse() throws Exception {
        when(userPersistenceAdapter.save(new UserEntity(userId, password, mobileNo))).thenReturn("UserId: " + userId + " Password: " + password + " MobileNo: " + mobileNo);
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8081/v1/user/register")
                .param("userId", userId)
                .param("password", password)
                .param("mobileNo", mobileNo)
                .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }

    @Test
    void GivenUserCredentianls_WhenUpdateUserIsCalled_ShouldReturnResponse() throws Exception {
        when(userPersistenceAdapter.update(new UserEntity(userId, password, mobileNo))).thenReturn("UserId: " + userId + " Password: " + "222222" + " MobileNo: " + mobileNo);
        mockMvc.perform(MockMvcRequestBuilders.post("http://localhost:8081/v1/user/register")
                        .param("userId", userId)
                        .param("password", "222222")
                        .param("mobileNo", mobileNo)
                        .accept(CONTENT_TYPE))
                .andExpect(status().isOk());
    }





}